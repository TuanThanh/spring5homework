package aptech.fpt.spring.service;

import aptech.fpt.spring.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
